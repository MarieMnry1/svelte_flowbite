# Zoo App using Svelte - SvelteKit & Flowbite
Trying to make a directory (un annuaire) of animals +
Add the SvelteKit router +
Installing a Svelte component's library called Flowbite

## Install the project
```bash
git clone git@gitlab.com:MarieMnry1/svelte_flowbite.git

npm install

npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

Thank you =)
Marie Maunoury